
## Patch Series

Patch series are two or more commits that will be submitted together in the same
e-mail for being linked some how. It is common to add a cover letter to a patch
series to give the main idea of the series.


## Editing a patch

In the most of the time, the community will give you feedbacks to improve your
patch. In that case, you shall make the modifications required and send a V2
from your patch.

Make all the modifications intended to and commit them:

```bash
git add <the_files_you_changed>
git commit -s
```

In the commit message, add the changes made in this version, check other commits
to be sure how to do it:

```diff
Changes in v2:
	first change
	second change
```

### Adding the changes to former commit

Change the former commit using the rebase command:

```bash
git rebase -i <your_former_commit_id>^
```

There are some rebase options, let's use the 'squash: meld the commit into
previous' which allows us to keep the former commit's message and add the new.


## Making a file.patch

To make files with all patches made, use format-patch:

For a single patch:

```bash
git format-patch -1 <commit_id> -v<version>
```

For a patch series:

```bash
git format-patch -N --cover-letter <most_recent_commit_id> -v<version>
```

The <version> indicates the version of your patch. The 'N' indicates the number
of commits it is in a patch series.

The previous command will provide N <first_line_of_your_commit>.patch with a
[PATCH V<version>] header and a cover letter. The header is important so the
maintainers can keep track of each patch's versions. The cover letter must
contain a brief explanation of the series.


## Send it to the emails lists and maintainers.

```bash
git send-email *.patch --to "<lists>","<mantainers>","<lkcamp lists>"
```
